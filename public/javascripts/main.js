
var ViewModel = function () {
  var main = this;
  var categoriaUri = '/api/categoria';
  main.list = ko.observableArray();
  main.error = ko.observable();
  main.categoriaCargada = ko.observable();
  main.categoriaNueva = {
        Nombre: ko.observable()
    }
  main.cargar = function(item) {
    main.categoriaCargada(item);
  }

  main.agregar = function (formElement) {
    var nueva = {
      Nombre: main.categoriaNueva.Nombre()
    }
    console.log(JSON.stringify(nueva));
    ajaxHelper(categoriaUri, 'POST', nueva).done(function (data) {
      getAll();
    });
  }


  function ajaxHelper(uri, method, data) {
      main.error('');
      return $.ajax({
          url: uri,
          type: method,
          dataType: 'json',
          contentType: 'application/json',
          data: data ? JSON.stringify(data) : null
      }).fail(function (jqXHR, textStatus, errorThrown) {
          main.error(errorThrown);
      });
  }

  function getAll() {
      ajaxHelper(categoriaUri, 'GET')
      .done(function (data) {
          limpiar();
          alert(JSON.stringify(data));
          main.list(data);
      });
  }

  function limpiar() {
      main.categoriaNueva.Nombre(null);
      main.categoriaCargada(null);
  }

  getAll();
}

$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);

});
